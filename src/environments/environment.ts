// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiHost: 'https://cityn.cf',
  authorizeHost: 'http://5.165.236.226',
  selfHost: 'http://localhost:4200',
  // selfHost: 'https://city-n.ml',
  client_id: 3,
  secret: 'cd286202-fda8-4b16-bd27-a6702419c583',
  previousUrl: '',
  apiKeyFor2Gis: 'rupsoj6358',
  apiKeyForPhoto: '66e1dde879b482869943d2418910f677',
  imageUpload: 'https://api.imgbb.com/1/upload'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
