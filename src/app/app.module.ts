import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoaderComponent } from './components/blocks/loader/loader.component';
import { ToastComponent } from './components/blocks/toast/toast.component';
import { NgbButtonsModule, NgbModalModule, NgbPaginationModule, NgbPopoverModule, NgbRatingModule, NgbToastModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { OAuthComponent } from './components/pages/o-auth/o-auth.component';
import { AuthGuard } from './services/auth-guard/auth-guard.service';
import { TokenInterceptor } from './interceptors/TokenInterceptor';
import { HomeComponent } from './components/pages/home/home.component';
import { SearchComponent } from './components/pages/search/search.component';
import { LayoutComponent } from './components/blocks/layout/layout.component';
import { PlaceComponent } from './components/blocks/place/place.component';
import { AgmCoreModule } from '@agm/core';
import { MapComponent } from './components/blocks/map/map.component';
import { ExampleComponent } from './components/pages/example/example.component';
import { AuthDirective } from './directives/AuthDirective';
import { LoginDirective } from './directives/LoginDirective';
import { ExitComponent } from './components/pages/exit/exit.component';
import { LoginComponent } from './components/pages/login/login.component';
import { PlaceInfoComponent } from './components/pages/place-info/place-info.component';
import { InputFileComponent } from './components/blocks/input-file/input-file.component';
import { CreatePlaceComponent } from './components/pages/create-place/create-place.component';
import { CreateRouteComponent } from './components/pages/create-route/create-route.component';
import { ProfileComponent } from './components/pages/profile/profile.component';
import { MyPlaceComponent } from './components/blocks/my-place/my-place.component';
import { FavPlaceComponent } from './components/blocks/fav-place/fav-place.component';

@NgModule({
  declarations: [
    AppComponent,
    LoaderComponent,
    ToastComponent,
    OAuthComponent,
    HomeComponent,
    SearchComponent,
    LayoutComponent,
    PlaceComponent,
    MapComponent,
    ExampleComponent,
    AuthDirective,
    ExitComponent,
    LoginDirective,
    LoginComponent,
    PlaceInfoComponent,
    InputFileComponent,
    CreatePlaceComponent,
    CreateRouteComponent,
    ProfileComponent,
    MyPlaceComponent,
    FavPlaceComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbToastModule,
    NgbButtonsModule,
    NgbPaginationModule,
    NgbTypeaheadModule,
    NgbPopoverModule,
    NgbModalModule,
    NgbRatingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAswSfLedj6CAxBvfQYeRvdUCECLuakMK0'
    })
  ],
  providers: [
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
