export class AccessResponse {
    public AccessToken: string;
    public RefreshToken: string;
}
