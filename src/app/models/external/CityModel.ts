export class CityModel {
    public id: number;
    public name: string;
    public regionId: number;
}
