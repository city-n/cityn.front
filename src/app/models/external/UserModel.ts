import { CityModel } from './CityModel';

export class UserModel {
    public id: number;
    public firstName: string;
    public lastName: string;
    public patronymic: string;
    public eMail: string;
    public birthDay: Date;
    public city: CityModel;
}
