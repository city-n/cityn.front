export class TwoGisPlace {
    public address_comment: string;
    public address_name: string;
    public id: string;
    public name: string;
    public type: string;
}
