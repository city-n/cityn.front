import { PointModel } from './PointModel';

export class TwoGisItem {
    public full_name: string;
    public id: string;
    public point: PointModel;
    public subtype: string;
    public type: string;
}