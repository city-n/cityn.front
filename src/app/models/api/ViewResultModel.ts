import { PlaceModel } from "./PlaceModel";

export class ViewResultModel {
    public count: number;
    public result: PlaceModel[];
}