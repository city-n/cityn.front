export class TagModel {
    public uid: string;
    public title: string;
    public title_ru: string;
    public type: string;
}
