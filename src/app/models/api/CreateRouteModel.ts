export class CreateRouteModel {
    public title: string;
    public description: string;
    public accessibility: string;
    public passage_time: string;
    public city: string;
    public places: string[];
    public photos: string[];
    public tags: string[];
}
