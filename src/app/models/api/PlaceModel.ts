export class PlaceModel {
    public uid: string;
    public title: string;
    public description: string;
    public coord_x: number;
    public coord_y: number;
    public accessibility: string;
    public user_id: number;
    public photos: string[];
    public address: string;
    public passage_time: string;
    public city: string;
    public tags: string[];

    public average_rate: number;
    public rating_count: number;
    public interest_rate: number;
    public interest_rating_count: number;
    public comfort_rate: number;
    public comfort_rating_count: number;
    public description_rate: number;
    public description_rating_count: number;
}
