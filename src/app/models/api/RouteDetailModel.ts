import { PlaceModel } from './PlaceModel';

export class RouteDetailModel {
    public accessibility: string;
    public description: string;
    public photos: string[];
    public places: PlaceModel[];
    public title: string;
    public uid: string;
    public user_id: number;
    public passage_time: string;
    public city: string;
    public tags: string[];

    public average_rate: number;
    public rating_count: number;
    public interest_rate: number;
    public interest_rating_count: number;
    public comfort_rate: number;
    public comfort_rating_count: number;
    public description_rate: number;
    public description_rating_count: number;
}
