import { PlaceModel } from "./PlaceModel";

export class UserProfileModel {
    userId: number;
    username: string;
    first_name: string;
    last_name: string;
    email: string;
    phone: string;
    notification: boolean;
    created_places: PlaceModel[];
    created_routes: any;
    favorite_places: PlaceModel[];
    favorite_routes: any;
}
