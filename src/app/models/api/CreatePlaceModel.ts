export class CreatePlaceModel {
    public title: string;
    public description: string;
    public address: string;
    public coord_x: string;
    public coord_y: string;
    public accessibility: string;
    public photos: string[];
    public tags: string[];
}
