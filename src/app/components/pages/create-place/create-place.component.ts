import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CityModel } from 'src/app/models/external/CityModel';
import { RegionModel } from 'src/app/models/external/RegionModel';
import { TwoGisItem } from 'src/app/models/external/TwoGisItem';
import { Marker } from 'src/app/models/MarkerModel';
import { AuthExternalService } from 'src/app/services/auth-external-service/auth-external.service';
import { GeocodingService } from 'src/app/services/geocoding-service/geocoding.service';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { CreatePlaceModel } from 'src/app/models/api/CreatePlaceModel';
import { PlaceService } from 'src/app/services/place-service/place.service';
import { ToastService } from 'src/app/services/toast-service/toast.service';
import { PlaceModel } from 'src/app/models/api/PlaceModel';
import { Router } from '@angular/router';
import { TagModel } from 'src/app/models/api/TagModel';
import { TagService } from 'src/app/services/tag-service/tag.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-create-place',
  templateUrl: './create-place.component.html',
  styleUrls: ['./create-place.component.scss']
})
export class CreatePlaceComponent implements OnInit {

  loading = false;
  loadingTag = false;
  placeForm: FormGroup;
  tagForm: FormGroup;
  showMap = false;

  latitude: number;
  longitude: number;

  markers: Marker[];

  cities = new Array<string>();
  loadData = false;
  photos = new Array<string>();

  tags: TagModel[] = new Array<TagModel>();
  typeOfButtons = ['btn-primary', 'btn-secondary', 'btn-success', 'btn-danger', 'btn-warning', 'btn-info', 'btn-dark'];

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 2 ? []
        : this.cities.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )

  constructor(private fb: FormBuilder, private geocodingService: GeocodingService, private modalService: NgbModal,
    private externalService: AuthExternalService, private placeService: PlaceService,
    private toastService: ToastService, private router: Router, private tagService: TagService) { }

  ngOnInit() {
    this.loading = true;
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
      });
    }

    this.tagService.getTags('place').subscribe(x => {
      this.tags = x;

      setTimeout(() => {
        for (const tag of this.tags) {
          const index = Math.floor((Math.random() * this.typeOfButtons.length));
          const elem = document.getElementById(tag.uid);
          elem.classList.add(this.typeOfButtons[index]);
        }
      }, 1);
    });

    this.externalService.getRegions().subscribe((regions: RegionModel[]) => {
      for (const region of regions) {
        this.loading = true;
        this.externalService.getCities(region.id).subscribe((cities: CityModel[]) => {
          for (const city of cities) {
            this.cities.push(city.name);
          }
          this.loading = false;
        });
      }
    });

    this.placeForm = this.fb.group({
      titleInput: this.fb.control(null, [Validators.required]),
      cityInput: this.fb.control(null, [Validators.required]),
      addressInput: this.fb.control(null, [Validators.required]),
      describeInput: this.fb.control(null, [Validators.required])
    });
  }

  showmap(showing: boolean) {
    this.showMap = showing;

    this.markers = new Array<Marker>();
    const marker = new Marker();
    marker.draggable = true;
    marker.lat = this.latitude;
    marker.lng = this.longitude;
    this.markers.push(marker);
    this.setAddress();
  }

  changeMarkers(event: Marker[]) {
    this.markers = event;
    this.setAddress();
  }

  setAddress() {
    this.geocodingService.searchByCoordinates(this.markers[0].lat, this.markers[0].lng).subscribe((x: any) => {
      const items = x.result.items as TwoGisItem[];
      let address = items.filter(item => item.subtype === 'district')[0].full_name;
      address += ', ' + items.filter(item => item.subtype === 'living_area')[0].full_name;
      if (items.filter(item => item.type === 'building').length > 0) {
        address = items.filter(item => item.type === 'building')[0].full_name;
      }
      this.placeForm.controls.cityInput.setValue(items.filter(item => item.subtype === 'city')[0].full_name);
      this.placeForm.controls.addressInput.setValue(address);
    });
  }

  createPlace() {
    this.loadData = true;
    if (!this.showMap) {
      this.geocodingService.searchByAddress(this.placeForm.controls.cityInput.value + ','
        + this.placeForm.controls.addressInput.value).subscribe((x: any) => {
          const items = x.result.items as TwoGisItem[];
          const place = new CreatePlaceModel();
          place.accessibility = 'public';
          place.address = this.placeForm.controls.cityInput.value + ','
            + this.placeForm.controls.addressInput.value;
          place.coord_x = items[0].point.lat;
          place.coord_y = items[0].point.lon;
          place.description = this.placeForm.controls.describeInput.value;
          place.title = this.placeForm.controls.titleInput.value;
          place.photos = this.photos;
          this.sendPlace(place);
        });
      return;
    }

    const place = new CreatePlaceModel();
    place.accessibility = 'public';
    place.address = this.placeForm.controls.cityInput.value + ','
      + this.placeForm.controls.addressInput.value;
    place.coord_x = this.markers[0].lat.toString();
    place.coord_y = this.markers[0].lng.toString();
    place.description = this.placeForm.controls.describeInput.value;
    place.title = this.placeForm.controls.titleInput.value;
    place.photos = this.photos;
    this.sendPlace(place);
  }

  uploadFile(photo: string) {
    this.photos.push(photo);
  }

  sendPlace(place: CreatePlaceModel) {
    const selectedTag = new Array<TagModel>();
    for (const tag of this.tags) {
      const elem = document.getElementById(tag.uid).classList;

      if (elem.contains('disabled')) {
        selectedTag.push(tag);
      }
    }

    place.tags = selectedTag.map(x => x.uid);
    this.placeService.createPlace(place).subscribe((x: PlaceModel) => {
      this.loadData = false;
      this.toastService.showSuccess('Место успешно создано!');
      this.router.navigate(['place', x.uid], {
        queryParams: {
          'type': 'Место'
        }
      });
    });
  }

  selectTag(tag: TagModel) {
    document.getElementById(tag.uid).classList.toggle('disabled');
  }

  addNewTag(content) {
    this.tagForm = this.fb.group({
      titleInput: this.fb.control(null, [Validators.required]),
      titleEngInput: this.fb.control(null)
    });

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });
  }

  createTag() {
    this.loadingTag = true;
    this.tagService.createTag('place', this.tagForm.controls.titleInput.value, this.tagForm.controls.titleEngInput.value)
      .subscribe(x => {
        this.loadingTag = false;
        this.tags.push(x);
        this.modalService.dismissAll();
        this.toastService.showSuccess('Тег успешно добавлен!');

        setTimeout(() => {
          const index = Math.floor((Math.random() * this.typeOfButtons.length));
          const elem = document.getElementById(x.uid);
          elem.classList.add(this.typeOfButtons[index]);
        }, 1);
      });
  }
}
