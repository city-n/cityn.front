import { Component, OnInit } from '@angular/core';
import { PlaceModel } from 'src/app/models/api/PlaceModel';
import { PhotoModel } from 'src/app/models/external/PhotoModel';
import { Marker } from 'src/app/models/MarkerModel';
import { GeocodingService } from 'src/app/services/geocoding-service/geocoding.service';
import { PhotoService } from 'src/app/services/photo-service/photo.service';
import { PlaceService } from 'src/app/services/place-service/place.service';

@Component({
  selector: 'app-example',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.scss']
})
export class ExampleComponent implements OnInit {

  constructor(public placeService: PlaceService, public geocodingService: GeocodingService, public photoService: PhotoService) { }

  latitude: number;
  longitude: number;
  index = 1;
  addPlaceFlag = false;

  markers: Marker[];

  ngOnInit() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
      });
    }

    this.markers = new Array<Marker>();

    /*this.placeService.getAllPlaces().subscribe((places: PlaceModel[]) => {
      for (let i = 0; i < places.length; i++) {
        const marker = new Marker();
        marker.draggable = false;
        marker.info = places[i].description;
        marker.label = i.toString();
        marker.lat = places[i].coord_x;
        marker.lng = places[i].coord_y;
        marker.title = places[i].title;
        this.markers.push(marker);
      }*/

    /*this.geocodingService.searchByCoordinates('56.83767548912172', '60.60372546315193').subscribe(x => console.log(x));
    this.geocodingService.searchByAddress('Екатеринбург, плотинка').subscribe(x => console.log(x));*/
    //});
  }

  addPlace() {
    const marker = new Marker();
    marker.draggable = true;
    this.markers.push(marker);
    this.addPlaceFlag = true;
  }

  changeMarkers(event: Marker[]) {
    this.markers = event;
  }

  aprovePlace(marker: Marker) {
    marker.draggable = false;
    this.addPlaceFlag = false;
  }
}
