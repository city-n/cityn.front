import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccessResponse } from 'src/app/models/AccessResponse';
import { SessionInfoService } from 'src/app/services/session-service/session-info.service';
import { environment } from 'src/environments/environment';

@Component({
  /*selector: 'oAuth-page',*/
  templateUrl: './o-auth.component.html',
  /*styleUrls: ['./o-auth.component.scss']*/
})
export class OAuthComponent {

  accessToken: string;
  refreshToken: string;

  constructor(route: ActivatedRoute, private sessionService: SessionInfoService, private router: Router) {
    route.queryParams.subscribe(
      (queryParam: any) => {
        this.accessToken = queryParam.access_token;
        this.refreshToken = queryParam.refreshToken;

        this.SetSession();
      }
    );
  }


  private SetSession() {

    const accessResponse = new AccessResponse();
    accessResponse.AccessToken = this.accessToken;
    accessResponse.RefreshToken = this.refreshToken;

    this.sessionService.setSession(accessResponse);
    if (localStorage.getItem('returnUrl') != null) {
      const returnUrl = localStorage.getItem('returnUrl').replace('/', '');
      localStorage.removeItem('returnUrl');
      location.href = returnUrl;
    } else {
      location.href = 'home';
    }
  }
}
