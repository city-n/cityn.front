import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PlaceModel } from 'src/app/models/api/PlaceModel';
import { TagModel } from 'src/app/models/api/TagModel';
import { ViewResultModel } from 'src/app/models/api/ViewResultModel';
import { PlaceService } from 'src/app/services/place-service/place.service';
import { RouteService } from 'src/app/services/route-service/route.service';
import { TagService } from 'src/app/services/tag-service/tag.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  typeOfPlace: FormGroup;
  loading = false;
  places: PlaceModel[];
  tags: TagModel[] = new Array<TagModel>();
  selectedTags: TagModel[] = new Array<TagModel>();

  page = 1;
  pages: number;
  counts = 10;
  outputs = [5, 10, 20, 50, 100];
  pageOutputs = 2;
  typeOfButtons = ['btn-primary', 'btn-secondary', 'btn-success', 'btn-danger', 'btn-warning', 'btn-info', 'btn-dark'];

  constructor(private placeService: PlaceService, private routeService: RouteService,
    private fb: FormBuilder, private tagService: TagService) { }

  ngOnInit() {
    this.places = new Array<PlaceModel>();

    this.getRoutes();

    this.typeOfPlace = this.fb.group({
      type: 'route'
    });

    this.typeOfPlace.controls.type.valueChanges.subscribe((x: String) => {
      if (x === 'route') {
        this.getRoutes();
      }

      if (x === 'place') {
        this.getPlaces();
      }
    });
  }

  getRoutes() {
    this.loading = true;
    this.tags = new Array<TagModel>();
    this.selectedTags = new Array<TagModel>();
    this.routeService.getAllRoutes(this.page, this.counts, this.selectedTags.map(x => x.uid))
      .subscribe((result: ViewResultModel) => {
        this.places = result.result;
        this.pages = result.count;
        this.tagService.getTags('route').subscribe(x => {
          this.tags = x;
          this.loading = false;

          setTimeout(() => {
            for (const tag of this.tags) {
              const index = Math.floor((Math.random() * this.typeOfButtons.length));
              const elem = document.getElementById(tag.uid);
              elem.classList.add(this.typeOfButtons[index]);
            }
          }, 1);
        });
      });
  }

  getPlaces() {
    this.loading = true;
    this.tags = new Array<TagModel>();
    this.selectedTags = new Array<TagModel>();
    this.placeService.getAllPlaces(this.page, this.counts, this.selectedTags.map(x => x.uid)).subscribe((result: ViewResultModel) => {
      this.places = result.result;
      this.pages = result.count;

      this.tagService.getTags('place').subscribe(x => {
        this.tags = x;
        this.loading = false;

        setTimeout(() => {
          for (const tag of this.tags) {
            const index = Math.floor((Math.random() * this.typeOfButtons.length));
            const elem = document.getElementById(tag.uid);
            elem.classList.add(this.typeOfButtons[index]);
          }
        }, 1);
      });
    });
  }

  pageChange(page: number) {
    this.page = page;
    this.getData();
  }

  GetPageSymbol(page: number) {
    return this.outputs[page - 1];
  }

  ChangeOutput(page: number) {
    this.counts = this.outputs[page - 1];
    this.getData();
  }

  getData() {
    if (this.typeOfPlace.controls.type.value === 'route') {
      this.getRoutes();
    }

    if (this.typeOfPlace.controls.type.value === 'place') {
      this.getPlaces();
    }
  }

  selectTag(tag: TagModel) {
    this.tags = this.tags.filter(x => x.uid !== tag.uid);
    this.selectedTags.push(tag);

    setTimeout(() => {
      const index = Math.floor((Math.random() * this.typeOfButtons.length));
      const elem = document.getElementById(tag.uid);
      elem.classList.add(this.typeOfButtons[index]);
    }, 1);
  }

  delTag(tag: TagModel) {
    this.selectedTags = this.selectedTags.filter(x => x.uid !== tag.uid);
    this.tags.push(tag);

    setTimeout(() => {
      const index = Math.floor((Math.random() * this.typeOfButtons.length));
      const elem = document.getElementById(tag.uid);
      elem.classList.add(this.typeOfButtons[index]);
    }, 1);
  }

  setFilter() {
    this.page = 1;
    if (this.typeOfPlace.controls.type.value === 'route') {
      this.loading = true;
      this.routeService.getAllRoutes(this.page, this.counts, this.selectedTags.map(x => x.uid))
        .subscribe((result: ViewResultModel) => {
          this.places = result.result;
          this.pages = result.count;
          this.loading = false;
        });
    }

    if (this.typeOfPlace.controls.type.value === 'place') {
      this.loading = true;
      this.placeService.getAllPlaces(this.page, this.counts, this.selectedTags.map(x => x.uid)).subscribe((result: ViewResultModel) => {
        this.places = result.result;
        this.pages = result.count;
        this.loading = false;
      });
    }
  }

}
