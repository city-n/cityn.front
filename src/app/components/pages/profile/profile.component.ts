import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { PlaceModel } from 'src/app/models/api/PlaceModel';
import { UserProfileModel } from 'src/app/models/api/UserProfileModel';
import { UserModel } from 'src/app/models/external/UserModel';
import { AuthExternalService } from 'src/app/services/auth-external-service/auth-external.service';
import { PlaceService } from 'src/app/services/place-service/place.service';
import { RouteService } from 'src/app/services/route-service/route.service';
import { ToastService } from 'src/app/services/toast-service/toast.service';
import { UserService } from 'src/app/services/user-service/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  placeLoading = false;
  routeLoading = false;
  places: PlaceModel[];

  userInfo: UserModel;
  userProfile: UserProfileModel;

  userLoading = false;
  userProfileLoading = false;
  notificationLoading = false;

  constructor(private placeService: PlaceService, private routeService: RouteService, private toastService: ToastService,
    private userExternalService: AuthExternalService, private userService: UserService) { }

  ngOnInit() {
    this.placeLoading = true;
    this.routeLoading = true;
    this.userLoading = true;
    this.userProfileLoading = true;

    this.places = new Array<PlaceModel>();

    this.userExternalService.getUserInfo().subscribe((x: UserModel) => {
      this.userInfo = x;
      this.userLoading = false;
    });

    this.userService.getUserInfo().subscribe(x => {
      this.userProfile = x;
      this.userProfileLoading = false;
    });
  }

  notifications() {
    this.notificationLoading = true;
    if (this.userProfile.notification) {
      this.userService.deactivateNotifications().subscribe(() => {
        this.notificationLoading = false;
        this.toastService.showSuccess('Оповещения отключены!');
        this.userProfile.notification = false;
      }, (error: HttpErrorResponse) => {
        this.notificationLoading = false;
        this.toastService.showDanger(error.message);
      });
    } else {
      this.userService.activateNotifications().subscribe(() => {
        this.notificationLoading = false;
        this.toastService.showSuccess('Оповещения включены!');
        this.userProfile.notification = true;
      }, (error: HttpErrorResponse) => {
        this.notificationLoading = false;
        this.toastService.showDanger(error.message);
      });
    }
  }
}
