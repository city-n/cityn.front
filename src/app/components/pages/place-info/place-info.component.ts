import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PlaceModel } from 'src/app/models/api/PlaceModel';
import { RouteDetailModel } from 'src/app/models/api/RouteDetailModel';
import { TagModel } from 'src/app/models/api/TagModel';
import { Marker } from 'src/app/models/MarkerModel';
import { PlaceService } from 'src/app/services/place-service/place.service';
import { RouteService } from 'src/app/services/route-service/route.service';
import { SessionInfoService } from 'src/app/services/session-service/session-info.service';
import { TagService } from 'src/app/services/tag-service/tag.service';
import { ToastService } from 'src/app/services/toast-service/toast.service';

@Component({
  selector: 'place-info-page',
  templateUrl: './place-info.component.html',
  styleUrls: ['./place-info.component.scss']
})
export class PlaceInfoComponent implements OnInit {

  id: string;
  type: string;
  loading = false;
  comfortRateLoading = false;
  interestRateLoading = false;
  descriptionRateLoading = false;

  placeInfo: PlaceModel;
  route: RouteDetailModel;
  saveLoad = false;
  comfort_rate = 0;
  description_rate = 0;
  interest_rate = 0;

  markers: Marker[];

  tags = new Map<string, TagModel>();
  typeOfButtons = ['btn-primary', 'btn-secondary', 'btn-success', 'btn-danger', 'btn-warning', 'btn-info', 'btn-dark'];

  constructor(private activateRoute: ActivatedRoute, private routeService: RouteService, private placeService: PlaceService,
    private tagService: TagService, public sessionService: SessionInfoService, private toastService: ToastService,
    private modalService: NgbModal) {
  }

  ngOnInit() {
    this.markers = new Array<Marker>();
    this.loading = true;
    this.id = this.activateRoute.snapshot.params['id'];
    this.type = this.activateRoute.snapshot.queryParams['type'];
    let typeEng = '';
    if (this.type === 'Место') {
      typeEng = 'place';
    } else {
      typeEng = 'route';
    }

    if (this.type === 'Место') {
      this.placeService.getPlace(this.id).subscribe((place: PlaceModel) => {
        this.placeInfo = place;

        const marker = new Marker();
        marker.lat = place.coord_x;
        marker.lng = place.coord_y;
        marker.title = place.title;
        marker.info = place.description;
        marker.draggable = false;
        marker.label = '1';
        this.markers.push(marker);

        this.tagService.getTags(typeEng).subscribe(x => {
          this.tags = new Map<string, TagModel>(x.map(v => [v.uid, v]));

          setTimeout(() => {
            for (const tag of x) {
              const index = Math.floor((Math.random() * this.typeOfButtons.length));
              const elem = document.getElementById(tag.uid + '_place');
              if (elem != null) {
                elem.classList.add(this.typeOfButtons[index]);
              }
            }
          }, 1);
        });

        this.loading = false;

        setTimeout(() => {
          document.getElementsByName('0').forEach(x => x.classList.add('active'));
        }, 1);
      });
    }

    if (this.type === 'Маршрут') {
      this.routeService.getRoute(this.id).subscribe((route: RouteDetailModel) => {
        this.route = route;

        for (let i = 0; i < route.places.length; i++) {
          const marker = new Marker();
          marker.lat = route.places[i].coord_x;
          marker.lng = route.places[i].coord_y;
          marker.title = route.places[i].title;
          marker.info = route.places[i].description;
          marker.draggable = false;
          marker.label = (i + 1).toString();
          this.markers.push(marker);
        }

        this.tagService.getTags(typeEng).subscribe(x => {
          this.tags = new Map<string, TagModel>(x.map(v => [v.uid, v]));

          setTimeout(() => {
            for (const tag of x) {
              const index = Math.floor((Math.random() * this.typeOfButtons.length));
              const elem = document.getElementById(tag.uid + '_place');
              if (elem != null) {
                elem.classList.add(this.typeOfButtons[index]);
              }
            }
          }, 1);
        });

        this.loading = false;
        setTimeout(() => {
          document.getElementsByName('0').forEach(x => x.classList.add('active'));
        }, 1);
      });
    }
  }

  addToFavorite() {
    this.saveLoad = true;
    if (this.type === 'Место') {
      this.placeService.addToFavorite(this.placeInfo.uid).subscribe(() => {
        this.toastService.showSuccess('Место успешно добавлено в избранное!');
        this.saveLoad = false;
      });
    } else {
      this.routeService.addToFavorite(this.route.uid).subscribe(() => {
        this.toastService.showSuccess('Маршрут успешно добавлен в избранное!');
        this.saveLoad = false;
      });
    }
  }

  openModal(content) {
    this.description_rate = 0;
    this.interest_rate = 0;
    this.comfort_rate = 0;
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });
  }

  sendRate() {
    if (this.comfort_rate !== 0) {
      this.comfortRateLoading = true;
    }
    if (this.interest_rate !== 0) {
      this.interestRateLoading = true;
    }
    if (this.description_rate !== 0) {
      this.descriptionRateLoading = true;
    }

    if (this.type === 'Место') {
      if (this.descriptionRateLoading) {
        this.placeService.ratePlace(this.placeInfo.uid, this.description_rate, 'description').subscribe(() => {
          this.descriptionRateLoading = false;
        });
      }

      if (this.interestRateLoading) {
        this.placeService.ratePlace(this.placeInfo.uid, this.interest_rate, 'interest').subscribe(() => {
          this.interestRateLoading = false;
        });
      }

      if (this.comfortRateLoading) {
        this.placeService.ratePlace(this.placeInfo.uid, this.comfort_rate, 'comfort').subscribe(() => {
          this.comfortRateLoading = false;
        });
      }
    } else {
      if (this.descriptionRateLoading) {
        this.routeService.rateRoute(this.route.uid, this.description_rate, 'description').subscribe(() => {
          this.descriptionRateLoading = false;
        });
      }

      if (this.interestRateLoading) {
        this.routeService.rateRoute(this.route.uid, this.interest_rate, 'interest').subscribe(() => {
          this.interestRateLoading = false;
        });
      }

      if (this.comfortRateLoading) {
        this.routeService.rateRoute(this.route.uid, this.comfort_rate, 'comfort').subscribe(() => {
          this.comfortRateLoading = false;
        });
      }
    }

    setTimeout(() => {
      this.modalService.dismissAll();

      if (this.type === 'Место') {
        this.toastService.showSuccess('Место успешно оценено!');
        this.placeInfo.rating_count += 1;
        if (this.description_rate !== 0) {
          this.placeInfo.description_rate = (this.placeInfo.description_rate * this.placeInfo.description_rating_count
            + this.description_rate) / (this.placeInfo.description_rating_count + 1);
        }
        if (this.comfort_rate !== 0) {
          this.placeInfo.comfort_rate = (this.placeInfo.comfort_rate * this.placeInfo.comfort_rating_count
            + this.comfort_rate) / (this.placeInfo.comfort_rating_count + 1);
        }
        if (this.interest_rate !== 0) {
          this.placeInfo.interest_rate = (this.placeInfo.interest_rate * this.placeInfo.interest_rating_count
            + this.interest_rate) / (this.placeInfo.interest_rating_count + 1);
        }
        this.placeInfo.average_rate = (this.placeInfo.interest_rate + this.placeInfo.description_rate
          + this.placeInfo.comfort_rate) / 3;
      } else {
        this.toastService.showSuccess('Маршрут успешно оценен!');
        this.route.rating_count += 1;
        if (this.description_rate !== 0) {
          this.route.description_rate = (this.route.description_rate * this.route.description_rating_count
            + this.description_rate) / (this.route.description_rating_count + 1);
        }
        if (this.comfort_rate !== 0) {
          this.route.comfort_rate = (this.route.comfort_rate * this.route.comfort_rating_count
            + this.comfort_rate) / (this.route.comfort_rating_count + 1);
        }
        if (this.interest_rate !== 0) {
          this.route.interest_rate = (this.route.interest_rate * this.route.interest_rating_count
            + this.interest_rate) / (this.route.interest_rating_count + 1);
        }
        this.route.average_rate = (this.route.interest_rate + this.route.description_rate
          + this.route.comfort_rate) / 3;
      }

    }, 2000);
  }

}
