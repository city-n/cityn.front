import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { CreateRouteModel } from 'src/app/models/api/CreateRouteModel';
import { PlaceModel } from 'src/app/models/api/PlaceModel';
import { TagModel } from 'src/app/models/api/TagModel';
import { ViewResultModel } from 'src/app/models/api/ViewResultModel';
import { CityModel } from 'src/app/models/external/CityModel';
import { RegionModel } from 'src/app/models/external/RegionModel';
import { Marker } from 'src/app/models/MarkerModel';
import { AuthExternalService } from 'src/app/services/auth-external-service/auth-external.service';
import { PlaceService } from 'src/app/services/place-service/place.service';
import { RouteService } from 'src/app/services/route-service/route.service';
import { TagService } from 'src/app/services/tag-service/tag.service';
import { ToastService } from 'src/app/services/toast-service/toast.service';

@Component({
  selector: 'app-create-route',
  templateUrl: './create-route.component.html',
  styleUrls: ['./create-route.component.scss']
})
export class CreateRouteComponent implements OnInit {

  loading = false;
  loadData = false;
  loadPlaces = false;
  loadingTag = false;

  cities = new Array<string>();
  photos = new Array<string>();
  places = new Array<PlaceModel>();
  selectPlaces = new Array<PlaceModel>();

  tagForm: FormGroup;
  routeForm: FormGroup;

  tags: TagModel[] = new Array<TagModel>();
  typeOfButtons = ['btn-primary', 'btn-secondary', 'btn-success', 'btn-danger', 'btn-warning', 'btn-info', 'btn-dark'];

  latitude: number;
  longitude: number;

  markers: Marker[];

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 2 ? []
        : this.cities.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )

  constructor(private externalService: AuthExternalService, private fb: FormBuilder, private routeService: RouteService,
    private placeService: PlaceService, private toastService: ToastService, private router: Router,
    private modalService: NgbModal, private tagService: TagService) { }

  ngOnInit() {
    this.externalService.getRegions().subscribe((regions: RegionModel[]) => {
      for (const region of regions) {
        this.loading = true;
        this.externalService.getCities(region.id).subscribe((cities: CityModel[]) => {
          for (const city of cities) {
            this.cities.push(city.name);
          }
          this.loading = false;
        });
      }
    });

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
      });
    }

    this.markers = new Array<Marker>();

    this.tagService.getTags('route').subscribe(x => {
      this.tags = x;

      setTimeout(() => {
        for (const tag of this.tags) {
          const index = Math.floor((Math.random() * this.typeOfButtons.length));
          const elem = document.getElementById(tag.uid);
          elem.classList.add(this.typeOfButtons[index]);
        }
      }, 1);
    });

    this.routeForm = this.fb.group({
      titleInput: this.fb.control(null, [Validators.required]),
      cityInput: this.fb.control(null, [Validators.required]),
      describeInput: this.fb.control(null, [Validators.required]),
      passageTimeInput: this.fb.control(null, [Validators.required])
    });
  }

  uploadFile(photo: string) {
    this.photos.push(photo);
  }

  createRoute() {
    this.loadData = true;
    const route = new CreateRouteModel();
    route.accessibility = 'public';
    route.city = this.routeForm.controls.cityInput.value;
    route.description = this.routeForm.controls.describeInput.value;
    route.passage_time = this.routeForm.controls.passageTimeInput.value;
    route.photos = this.photos;
    route.places = this.selectPlaces.map(x => x.uid);
    route.title = this.routeForm.controls.titleInput.value;

    const selectedTag = new Array<TagModel>();
    for (const tag of this.tags) {
      const elem = document.getElementById(tag.uid).classList;

      if (elem.contains('disabled')) {
        selectedTag.push(tag);
      }
    }

    route.tags = selectedTag.map(x => x.uid);

    this.routeService.createRoute(route).subscribe((x: PlaceModel) => {
      this.loadData = false;
      this.toastService.showSuccess('Маршрут успешно добавлен!');
      this.router.navigate(['place', x.uid], {
        queryParams: {
          'type': 'Маршрут'
        }
      });
    });
  }

  openModal(content) {
    this.loadPlaces = true;
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });
    this.placeService.getAllPlaces(1, 1000, null).subscribe((places: ViewResultModel) => {
      const uids = this.selectPlaces.map(x => x.uid);
      this.places = places.result.filter(x => !uids.includes(x.uid));
      this.loadPlaces = false;
      setTimeout(() => {
        document.getElementsByName('0').forEach(x => x.classList.add('active'));
      }, 100);
    });
  }

  select(place: PlaceModel) {
    this.selectPlaces.push(place);
    this.places = this.places.filter(x => x.uid !== place.uid);

    const marker = new Marker();
    marker.lat = place.coord_x;
    marker.lng = place.coord_y;
    marker.title = place.title;
    marker.draggable = false;
    marker.info = place.description;
    marker.label = this.selectPlaces.length.toString();
    this.markers.push(marker);

    setTimeout(() => {
      document.getElementsByName('0').forEach(x => x.classList.add('active'));
    }, 100);

    console.log(this.selectPlaces);
  }

  remove(selectPlace: PlaceModel) {
    this.selectPlaces = this.selectPlaces.filter(x => x !== selectPlace);
    this.markers = this.markers.filter(x => x.lat !== selectPlace.coord_x && x.lng !== selectPlace.coord_y);

    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].label = (i + 1).toString();
    }
  }

  selectTag(tag: TagModel) {
    document.getElementById(tag.uid).classList.toggle('disabled');
  }

  addNewTag(content) {
    this.tagForm = this.fb.group({
      titleInput: this.fb.control(null, [Validators.required]),
      titleEngInput: this.fb.control(null)
    });

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });
  }

  createTag() {
    this.loadingTag = true;
    this.tagService.createTag('route', this.tagForm.controls.titleInput.value, this.tagForm.controls.titleEngInput.value)
      .subscribe(x => {
        this.loadingTag = false;
        this.tags.push(x);
        this.modalService.dismissAll();
        this.toastService.showSuccess('Тег успешно добавлен!');

        setTimeout(() => {
          const index = Math.floor((Math.random() * this.typeOfButtons.length));
          const elem = document.getElementById(x.uid);
          elem.classList.add(this.typeOfButtons[index]);
        }, 1);
      });
  }

}
