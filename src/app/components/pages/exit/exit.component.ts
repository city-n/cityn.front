import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionInfoService } from 'src/app/services/session-service/session-info.service';

@Component({
  selector: 'exit-page',
  templateUrl: './exit.component.html',
  styleUrls: ['./exit.component.scss']
})
export class ExitComponent implements OnInit {

  constructor(private sessionService: SessionInfoService, private router: Router) { }

  ngOnInit() {
    this.sessionService.logout();
    location.href = 'home';
  }

}
