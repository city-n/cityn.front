import { Component, OnInit } from '@angular/core';
import { AuthExternalService } from 'src/app/services/auth-external-service/auth-external.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'login-page',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private externalService: AuthExternalService) { }

  ngOnInit() {
    location.href = this.externalService.getAuthorizeUrl();
    localStorage.setItem('returnUrl', environment.previousUrl);
  }

}
