import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PlaceModel } from 'src/app/models/api/PlaceModel';
import { PlaceService } from 'src/app/services/place-service/place.service';
import { RouteService } from 'src/app/services/route-service/route.service';
import { ToastService } from 'src/app/services/toast-service/toast.service';

@Component({
  selector: 'fav-place',
  templateUrl: './fav-place.component.html',
  styleUrls: ['./fav-place.component.scss']
})
export class FavPlaceComponent implements OnInit {

  @Input() favouritePlaces: PlaceModel[];
  @Input() favouriteRoutes: PlaceModel[];

  constructor(private router: Router, private routeService: RouteService, private placeService: PlaceService,
    private toastService: ToastService) { }

  ngOnInit() {
    setTimeout(() => {
      document.getElementsByName('0').forEach(x => x.classList.add('active'));
    }, 100);
  }

  redirect(place: PlaceModel, type: string) {
    this.router.navigate(['place', place.uid], {
      queryParams: {
        'type': type
      }
    });
  }

  delete(place: PlaceModel, type: string) {
    if (type === 'Место') {
      this.placeService.removeFromFavorite(place.uid).subscribe(() => {
        this.favouritePlaces = this.favouritePlaces.filter(x => x.uid !== place.uid);
        this.toastService.showSuccess('Место удалено из избранного!');
      });
    } else {
      this.routeService.removeFromFavorite(place.uid).subscribe(() => {
        this.favouriteRoutes = this.favouriteRoutes.filter(x => x.uid !== place.uid);
        this.toastService.showSuccess('Маршрут удалено из избранного!');
      });
    }
  }

}
