import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavPlaceComponent } from './fav-place.component';

describe('FavPlaceComponent', () => {
  let component: FavPlaceComponent;
  let fixture: ComponentFixture<FavPlaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavPlaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavPlaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
