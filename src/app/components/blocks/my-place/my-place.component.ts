import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PlaceModel } from 'src/app/models/api/PlaceModel';
import { PlaceService } from 'src/app/services/place-service/place.service';
import { RouteService } from 'src/app/services/route-service/route.service';
import { ToastService } from 'src/app/services/toast-service/toast.service';

@Component({
  selector: 'my-place',
  templateUrl: './my-place.component.html',
  styleUrls: ['./my-place.component.scss']
})
export class MyPlaceComponent implements OnInit {

  @Input() myPlaces: PlaceModel[];
  @Input() myRoutes: PlaceModel[];

  constructor(private router: Router) { }

  ngOnInit() {
    setTimeout(() => {
      document.getElementsByName('0').forEach(x => x.classList.add('active'));
    }, 100);
  }

  redirect(place: PlaceModel, type: string) {
    this.router.navigate(['place', place.uid], {
      queryParams: {
        'type': type
      }
    });
  }

}
