import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PhotoModel } from 'src/app/models/external/PhotoModel';
import { PhotoService } from 'src/app/services/photo-service/photo.service';
import { ToastService } from 'src/app/services/toast-service/toast.service';

@Component({
  selector: 'input-file-block',
  templateUrl: './input-file.component.html',
  styleUrls: ['./input-file.component.scss']
})
export class InputFileComponent implements OnInit {

  @Output() uploadFile = new EventEmitter<string>();

  image: string;
  name = 'Выберите изображение...';
  loading = false;
  fileNull = true;

  constructor(private photoService: PhotoService, private toastService: ToastService) { }

  ngOnInit() {
  }

  getBase64(event) {
    this.image = null;
    this.fileNull = false;

    let file = event.target.files[0];
    this.name = file.name;
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.image = reader.result.toString();
      this.image = this.image.slice(this.image.indexOf(',') + 1);
    };
  }

  sendImage() {
    this.loading = true;
    this.photoService.uploadPhoto(this.image)
      .subscribe((x: PhotoModel) => {
        this.toastService.showSuccess('Изображение успешно добавлено!');
        this.uploadFile.emit(x.data.url);
        this.loading = false;
        this.name = 'Выберите изображение...';
        this.fileNull = true;
      },
        (error: HttpErrorResponse) => {
          this.loading = false;
          this.toastService.showDanger(error.message);
        });
  }

}
