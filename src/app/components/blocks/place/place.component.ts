import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PlaceModel } from 'src/app/models/api/PlaceModel';
import { TagModel } from 'src/app/models/api/TagModel';
import { TagService } from 'src/app/services/tag-service/tag.service';

@Component({
  selector: 'place-block',
  templateUrl: './place.component.html',
  styleUrls: ['./place.component.scss']
})
export class PlaceComponent implements OnInit {

  @Input() place: PlaceModel;
  tags = new Map<string, TagModel>();

  typeOfButtons = ['btn-primary', 'btn-secondary', 'btn-success', 'btn-danger', 'btn-warning', 'btn-info', 'btn-dark'];

  constructor(private router: Router, private tagService: TagService) { }

  ngOnInit() {
    this.place.photos = this.place.photos.filter(x => x != null || x !== '');
    setTimeout(() => {
      document.getElementsByName('0').forEach(x => x.classList.add('active'));
    }, 100);
    let type = '';
    if (this.place.city == null) {
      type = 'place';
    } else {
      type = 'route';
    }

    this.tagService.getTags(type).subscribe(x => {
      this.tags = new Map<string, TagModel>(x.map(v => [v.uid, v]));

      setTimeout(() => {
        for (const tag of x) {
          const index = Math.floor((Math.random() * this.typeOfButtons.length));
          const elem = document.getElementById(tag.uid + '_place_' + this.place.uid);
          if (elem != null) {
            elem.classList.add(this.typeOfButtons[index]);
          }
        }
      }, 1);
    });
  }

  detailed(id: string) {
    let type = '';
    if (this.place.city == null) {
      type = 'Место';
    } else {
      type = 'Маршрут';
    }
    this.router.navigate(['place', id], {
      queryParams: {
        'type': type
      }
    });
  }

}
