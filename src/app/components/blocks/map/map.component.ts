import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Marker } from 'src/app/models/MarkerModel';

@Component({
  selector: 'map-block',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  zoom = 12;

  @Input() lat: number;
  @Input() lng: number;

  @Input() width: number;
  @Input() height: number;

  @Input() markers: Marker[];
  @Output() changeArrayOfMarkers = new EventEmitter<Marker[]>();

  constructor() { }

  ngOnInit() {

    document.getElementById('map').style.height = this.height.toString() + 'px';
    document.getElementById('map').style.width = this.width.toString() + 'px';

    if (this.markers != null && this.markers.length !== 0) {
      this.lat = 0;
      this.lng = 0;
      for (const marker of this.markers) {
        this.lat += Number.parseFloat(marker.lat.toString());
        this.lng += Number.parseFloat(marker.lng.toString());
      }

      this.lat = this.lat / this.markers.length;
      this.lng = this.lng / this.markers.length;
    }
  }

  clickedMarker(label: string, index: number) {
    //console.log(`clicked the marker: ${label || index}`);
  }

  markerDragEnd(m: Marker, event: any) {
    for (const marker of this.markers) {
      if (marker.draggable != null && marker.draggable === true) {
        marker.lng = event.coords.lng;
        marker.lat = event.coords.lat;
      }
    }

    this.changeArrayOfMarkers.emit(this.markers);
  }

  zoomChanged(zoom: number) {
    this.zoom = zoom;
    //console.log(this.zoom);
  }

  mapClicked(event) {
    for (const marker of this.markers) {
      if (marker.draggable != null && marker.draggable === true) {
        marker.lat = event.coords.lat;
        marker.lng = event.coords.lng;
      }
    }

    this.changeArrayOfMarkers.emit(this.markers);
  }

}
