import { Directive, EmbeddedViewRef, TemplateRef, ViewContainerRef } from "@angular/core";
import { SessionInfoService } from '../services/session-service/session-info.service';

@Directive({
    selector: '[isLogin]'
})
export class AuthDirective {

    private viewRef: EmbeddedViewRef<any> | null = null;

    constructor(private templateRef: TemplateRef<any>, private viewContainerRef: ViewContainerRef,
        private sessionService: SessionInfoService) {
        this.update();
    }

    private update() {
        if (this.sessionService.isLoggedIn()) {
            if (this.viewRef == null || this.viewRef.destroyed) {
                this.viewRef = this.viewContainerRef.createEmbeddedView(this.templateRef);
            }
        } else if (this.viewRef != null) {
            this.viewRef.destroy();
        }
    }
}