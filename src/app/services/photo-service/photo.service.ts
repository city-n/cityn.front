import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  url = environment.imageUpload + '?key=' + environment.apiKeyForPhoto;

  constructor(private http: HttpClient) { }

  uploadPhoto(base64: string) {
    const formData = new FormData();
    formData.append('image', base64);
    return this.http.post(this.url, formData);
  }
}
