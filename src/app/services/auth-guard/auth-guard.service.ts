import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AuthExternalService } from '../auth-external-service/auth-external.service';
import { SessionInfoService } from '../session-service/session-info.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private sessionService: SessionInfoService, private router: Router,
    private externalService: AuthExternalService) { }

  canActivate(route: ActivatedRouteSnapshot) {
    if (this.sessionService.isLoggedIn()) {
      return true;
    } else {
      location.href = this.externalService.getAuthorizeUrl();
      this.sessionService.logout();
      return false;
    }
  }
}
