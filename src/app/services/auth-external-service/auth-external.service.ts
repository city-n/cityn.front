import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthExternalService {

  constructor(private http: HttpClient) { }

  public getUserInfo() {
    return this.http.get(environment.authorizeHost + '/resource/get/get_user');
  }

  public refreshToken() {
    return this.http.get(environment.authorizeHost + '/auth/refresh_token');
  }

  public getAuthorizeUrl() {
    return environment.authorizeHost + '/oAuth?client_id=' + environment.client_id + '&secret='
      + environment.secret + '&scope=1,2&redirect_uri=' + environment.selfHost + '/oAuth';
  }

  public getRegions() {
    return this.http.get(environment.authorizeHost + '/resource/public/get_regions?country=0');
  }

  public getCities(region: number) {
    return this.http.get(environment.authorizeHost + '/resource/public/get_cities?region=' + region);
  }
}
