import { TestBed } from '@angular/core/testing';

import { AuthExternalService } from './auth-external.service';

describe('ExternalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthExternalService = TestBed.get(AuthExternalService);
    expect(service).toBeTruthy();
  });
});
