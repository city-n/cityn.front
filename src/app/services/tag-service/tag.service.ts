import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TagModel } from 'src/app/models/api/TagModel';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(private http: HttpClient) { }

  public getTags(type: string): Observable<TagModel[]> {
    return this.http.get<TagModel[]>(environment.apiHost + '/tag?type=' + type);
  }

  public getTag(uid: string): Observable<TagModel> {
    return this.http.get<TagModel>(environment.apiHost + '/' + uid);
  }

  public createTag(type: string, title: string, titleEn: string): Observable<TagModel> {
    const formData = new FormData();
    formData.append('type', type);
    formData.append('title_ru', title);
    if (titleEn != null && titleEn !== '') {
      formData.append('title', titleEn);
    }

    return this.http.post<TagModel>(environment.apiHost + '/tag/', formData);
  }
}
