import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GeocodingService {

  apiUrl = 'https://catalog.api.2gis.com/3.0/items/geocode?';
  partialUrl = '&fields=items.point&key=' + environment.apiKeyFor2Gis;
  placeApi = 'https://catalog.api.2gis.com/3.0/items?key=' + environment.apiKeyFor2Gis + '&q=';

  constructor(private http: HttpClient) { }

  public searchByCoordinates(lat: number, lng: number) {
    return this.http.get(this.apiUrl + 'lat=' + lat + '&lon=' + lng + this.partialUrl);
  }

  public searchByAddress(address: string) {
    return this.http.get(this.apiUrl + 'q=' + address + this.partialUrl);
  }

  public searchAddress(address: string) {
    return this.http.get(this.placeApi + address);
  }
}
