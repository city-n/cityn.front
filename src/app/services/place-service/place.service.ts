import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreatePlaceModel } from 'src/app/models/api/CreatePlaceModel';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlaceService {

  constructor(private http: HttpClient) { }

  public getAllPlaces(page: number, count: number, tags: string[]) {
    let url = environment.apiHost + '/place/?page_size=' + count + '&page_num=' + page;
    if (tags != null && tags.length > 0) {
      url += '&tags=' + tags.toString();
    }
    return this.http.get(url);
  }

  public getPlace(uid: string) {
    return this.http.get(environment.apiHost + '/place/' + uid + '/');
  }

  public createPlace(place: CreatePlaceModel) {
    const formData = new FormData();
    formData.append('accessibility', place.accessibility);
    formData.append('address', place.address);
    formData.append('coord_x', place.coord_x);
    formData.append('coord_y', place.coord_y);
    formData.append('description', place.description);
    formData.append('photos', place.photos.toString());
    formData.append('title', place.title);
    formData.append('tags', place.tags.toString());

    return this.http.post(environment.apiHost + '/place/', formData);
  }

  public addToFavorite(uid: string): Observable<void> {
    return this.http.post<void>(environment.apiHost + '/place/' + uid + '/add_favorite/', new FormData());
  }

  public removeFromFavorite(uid: string): Observable<void> {
    return this.http.post<void>(environment.apiHost + '/place/' + uid + '/remove_favorite/', new FormData());
  }

  public ratePlace(uid: string, rate: number, criteria: string): Observable<void> {
    const formData = new FormData();
    formData.append('rate', rate.toString());
    formData.append('criteria', criteria);
    return this.http.post<void>(environment.apiHost + '/place/' + uid + '/rate_place/', formData);
  }
}
