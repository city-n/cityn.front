import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserProfileModel } from 'src/app/models/api/UserProfileModel';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  public getUserInfo(): Observable<UserProfileModel> {
    return this.http.get<UserProfileModel>(environment.apiHost + '/users/profile/');
  }

  public deactivateNotifications(): Observable<void> {
    return this.http.post<void>(environment.apiHost + '/users/notification_off/', new FormData());
  }

  public activateNotifications(): Observable<void> {
    return this.http.post<void>(environment.apiHost + '/users/notification_on/', new FormData());
  }
}
