import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreateRouteModel } from 'src/app/models/api/CreateRouteModel';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RouteService {

  constructor(private http: HttpClient) { }

  public getAllRoutes(page: number, count: number, tags: string[]) {
    let url = environment.apiHost + '/route?page_num=' + page + '&page_size=' + count;
    if (tags != null && tags.length > 0) {
      url += '&tags=' + tags.toString();
    }
    return this.http.get(url);
  }

  public getRoute(id: string) {
    return this.http.get(environment.apiHost + '/route/' + id);
  }

  public createRoute(route: CreateRouteModel) {
    const formData = new FormData();
    formData.append('accessibility', route.accessibility);
    formData.append('city', route.city);
    formData.append('description', route.description);
    formData.append('passage_time', route.passage_time);

    if (route.photos !== null && route.photos.length > 0) {
      formData.append('photos', route.photos.toString());
    }

    if (route.places !== null && route.places.length > 0) {
      formData.append('places', route.places.toString());
    }

    if (route.tags !== null && route.tags !== undefined && route.tags.length > 0) {
      formData.append('tags', route.tags.toString());
    }

    formData.append('title', route.title);

    return this.http.post(environment.apiHost + '/route/', formData);
  }

  public addToFavorite(uid: string): Observable<void> {
    return this.http.post<void>(environment.apiHost + '/route/' + uid + '/add_favorite/', new FormData());
  }

  public removeFromFavorite(uid: string): Observable<void> {
    return this.http.post<void>(environment.apiHost + '/route/' + uid + '/remove_favorite/', new FormData());
  }

  public rateRoute(uid: string, rate: number, criteria: string): Observable<void> {
    const formData = new FormData();
    formData.append('rate', rate.toString());
    formData.append('criteria', criteria);
    return this.http.post<void>(environment.apiHost + '/route/' + uid + '/rate_route/', formData);
  }
}
