import { Injectable } from '@angular/core';
import { AccessResponse } from 'src/app/models/AccessResponse';

@Injectable({
  providedIn: 'root'
})
export class SessionInfoService {

  constructor() { }

  private tokenLSKeyName = 'access_token';
  private refreshTokenLSKeyName = 'refresh_token';

  // Токен доступа
  public get accessToken(): string {
    return localStorage.getItem(this.tokenLSKeyName);
  }

  // Токен доступа
  public set accessToken(val: string) {
    localStorage.setItem(this.tokenLSKeyName, val);
  }

  // Токен обновления
  public get refreshToken(): string {
    return localStorage.getItem(this.refreshTokenLSKeyName);
  }

  // Токен обновления
  public set refreshToken(val: string) {
    localStorage.setItem(this.refreshTokenLSKeyName, val);
  }

  // Установить текущий токен
  public setSession(token: AccessResponse) {
    this.accessToken = token.AccessToken;
    this.refreshToken = token.RefreshToken;
  }

  public logout() {
    localStorage.clear();
  }

  // Проверка входа пользователя
  public isLoggedIn() {
    if (this.accessToken == null) {
      return false;
    }

    return true;
  }
}
