import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OAuthComponent } from './components/pages/o-auth/o-auth.component';
import { AuthGuard } from './services/auth-guard/auth-guard.service';
import { HomeComponent } from './components/pages/home/home.component';
import { SearchComponent } from './components/pages/search/search.component';
import { ExampleComponent } from './components/pages/example/example.component';
import { ExitComponent } from './components/pages/exit/exit.component';
import { LoginComponent } from './components/pages/login/login.component';
import { PlaceInfoComponent } from './components/pages/place-info/place-info.component';
import { CreatePlaceComponent } from './components/pages/create-place/create-place.component';
import { CreateRouteComponent } from './components/pages/create-route/create-route.component';
import { ProfileComponent } from './components/pages/profile/profile.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'oAuth', component: OAuthComponent },
  { path: 'home', component: HomeComponent },
  { path: 'search', component: SearchComponent },
  { path: 'example', component: ExampleComponent/*, canActivate: [AuthGuard]*/ },
  { path: 'place/:id', component: PlaceInfoComponent },
  { path: 'exit', component: ExitComponent },
  { path: 'login', component: LoginComponent },
  { path: 'create-place', component: CreatePlaceComponent },
  { path: 'create-route', component: CreateRouteComponent },
  { path: 'profile', component: ProfileComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
