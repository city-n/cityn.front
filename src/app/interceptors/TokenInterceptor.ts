import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { SessionInfoService } from '../services/session-service/session-info.service';
import { catchError, switchMap } from 'rxjs/operators';
import { AccessResponse } from '../models/AccessResponse';
import { AuthExternalService } from '../services/auth-external-service/auth-external.service';
import { Router } from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private session: SessionInfoService, private externalService: AuthExternalService, private router: Router) { }

    private authUrl = this.externalService.getAuthorizeUrl();

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const token = this.session.accessToken;
        const refreshToken = this.session.refreshToken;

        if (req.url.indexOf('https://api.imgbb.com/1/upload') >= 0) {
            return next.handle(req).pipe(catchError(err => this.handleError(err, req, next)));
        }

        if (token) {
            req = this.addToken(req, token);
        }

        if (refreshToken && req.url.indexOf('auth/refresh') > -1) {
            req = this.addRefreshToken(req, token, refreshToken);
        }

        return next.handle(req).pipe(catchError(err => this.handleError(err, req, next)));
    }

    private handleError(error: any, request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (error instanceof HttpErrorResponse) {
            switch ((error as HttpErrorResponse).status) {
                case 401:
                    if (this.isRefreshRequest(request)) {
                        localStorage.clear();
                        location.href = this.authUrl;
                        return throwError(error);
                    }
                    return this.refreshToken(request, next);
                default:
                    return throwError(error);
            }
        }
    }

    refreshToken(req: HttpRequest<any>, next: HttpHandler) {

        return this.externalService.refreshToken().pipe(switchMap((newToken: AccessResponse) => {
            this.session.setSession(newToken);
            return next.handle(this.addToken(req, newToken.AccessToken));
        }));
    }

    private isRefreshRequest(request: HttpRequest<any>): boolean {
        return request.url.indexOf('auth/refresh') > -1;
    }

    private addToken(req: HttpRequest<any>, accessToken: string): HttpRequest<any> {
        return req.clone({
            setHeaders: {
                token: accessToken,
                Authorization: 'token ' + accessToken,
            }
        });
    }

    private addRefreshToken(req: HttpRequest<any>, accessToken: string, refreshToken: string): HttpRequest<any> {
        return req.clone({
            setHeaders: {
                token: accessToken,
                'refresh-token': refreshToken
            }
        });
    }
}
